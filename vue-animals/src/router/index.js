import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import addAnimal from '@/components/addAnimal'
import Animals from '@/views/Animals.vue'
import Cats from '@/views/Cats.vue'
import Dogs from '@/views/Dogs.vue'
import Pet from '@/views/Pet.vue'
import Widgets from '@/views/Widgets.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },

    {
      path: '/animals',
      name: 'Animals',
      component: Animals
    },

    {
      path: '/add_animal',
      name: 'addAnimal',
      component: addAnimal
    },

    {
      path: '/cats',
      name: 'Cats',
      component: Cats
    },

    {
      path: '/dogs',
      name: 'Dogs',
      component: Dogs
    },

    {
      path: '/pets/:species/:id',
      name: 'Pet',
      component: Pet
    },

    {
      path: '/widgets',
      name: 'Widgets',
      component: Widgets
    }

  ]
})
